This is just the basic code for detecting and locating hands 

Update 23.4.2023:

- The Module has been updated to handle the orientation detection and calculation and also angle detection and calculation.
- Angle of hand in respect to wrist detection and calculation completed. 


Update 30.4.2023:

- Module has been updated to handle grip holding and placement detection. 
